/** ===========================================================================
 *
 * Gulp configuration file.
 *
 * ========================================================================= */

'use strict';

/** ---------------------------------------------------------------------------
 * Load plugins.
 * ------------------------------------------------------------------------- */

var gulp = require('gulp'),
    browser = require('browser-sync').create(),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    minifyJS = require('gulp-uglify'),
    concatJS = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    imagemin = require('gulp-imagemin'),
    svgSprites = require('gulp-svg-sprites'),
    sequence = require('run-sequence'),
    clean = require('gulp-clean'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify');

/** ---------------------------------------------------------------------------
 * Regular tasks.
 * ------------------------------------------------------------------------- */

// Create a server with BrowserSync and watch for file changes.
gulp.task('server', function() {
    browser.init({
        // Inject CSS changes without the page being reloaded.
        injectChanges: true,

        // Project URL.
        proxy: 'http://local.hm2019.test/',

        // `true` Automatically open the browser with BrowserSync live server.
        // `false` Stop the browser from automatically opening.
        open: true,

        // The port.
        port: 3000

        // For a complete list of options, visit: https://www.browsersync.io/docs/options
    });

    // Watch for file changes.
    gulp.watch('source/sass/**/*.scss', ['sass']);
    gulp.watch('source/javascript/libs/**/*.js', ['watch-js']);
    gulp.watch(['source/images/**/*.{png,jpg,gif,svg,ico}', '!source/images/sprites/**'], ['watch-img']);
    gulp.watch('source/images/sprites/**', ['sprites']);
});

// Compiles Sass to CSS.
gulp.task('sass', function() {
    return gulp
        .src('source/sass/**/*.scss')
        .pipe(plumber({
            errorHandler: notify.onError({
                title: 'Gulp error in the <%= error.plugin %> plugin',
                message: '<%= error.message %>'
            })
        }))
        .pipe(sass({
            // Sass related options go here.
            // See more options here: https://github.com/sass/node-sass#options
            outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie 6-8']
        }))
        .pipe(gulp.dest('assets/css'))
        .pipe(browser.stream());
});

// Concatenate and minify JS.
gulp.task('js', ['lint-js'], function() {
    return gulp
        .src('source/javascript/libs/**/*.js')
        .pipe(concatJS('scripts.js'))
        .pipe(gulp.dest('assets/js'))
        .pipe(minifyJS({
            // For options visit: https://github.com/mishoo/UglifyJS2#minify-options
        }))
        .pipe(rename('scripts.min.js'))
        .pipe(gulp.dest('assets/js'));
});

// Check JS code for errors.
gulp.task('lint-js', function() {
    return gulp
        .src('source/javacsript/libs/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail')); // task fails on JSHint error
});

// Compresses images.
gulp.task('img',  function() {
    return gulp
        .src(['source/images/**/*.{png,jpg,gif,svg,ico}', '!source/images/sprites/**'])
        .pipe(imagemin({
            verbose: true
        }))
        .pipe(gulp.dest('assets/images'));
});

// Creates sprites from SVG files.
gulp.task('sprites', function() {
    return gulp
        .src('source/images/sprites/**/*.svg')
        .pipe(svgSprites({
            cssFile: 'sass/_sprites.scss',
            templates: {
                scss: true
            },
            preview: false,
            svg: {
                sprite: 'images/sprite.svg'
            }
        }))
        .pipe(gulp.dest('source'));
});

// Deletes the dist folder so the build can start fresh.
gulp.task('reset', function() {
    return gulp
        .src('dist')
        .pipe(clean());
});


/** ---------------------------------------------------------------------------
 * Watch tasks.
 * ------------------------------------------------------------------------- */

// PHP.
gulp.task('watch-php', ['php'], function(done){
    browser.reload();
    done();
});

// Images.
gulp.task('watch-img', ['img'], function(done){
    browser.reload();
    done();
});

// JS.
gulp.task('watch-js', ['js'], function(done) {
    browser.reload();
    done();
});

/** ---------------------------------------------------------------------------
 * The main task.
 * ------------------------------------------------------------------------- */
gulp.task('default', function(cb) {
    sequence('reset', 'sprites', ['sass', 'img', 'js'], 'server', cb);
});
