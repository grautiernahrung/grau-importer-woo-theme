# grau GmbH WooCommerce theme

The grau GmbH Woocommerce theme can be used by importers around the world who want to run their own webshop based on WooCommerce with the grau GmbH look-and-feel. Having the Storefront theme installed is a requirement to make this childtheme work. Apart from the mandatory items there are some optional plugins you can install to make the theme even work better.

## Installation

0. **ALWAYS MAKE A BACKUP. TEST IT IN A TEST ENVIRONMENT FIRST. NEVER INSTALL ON A PRODUCTION SITE WITHOUT TESTING**
1. [Download WooCommerce StoreFront Theme](https://wordpress.org/themes/storefront/)
2. Goto WordPress > Appearance > Themes > Add New.
3. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
4. [Download the grau GmbH Childtheme](https://bitbucket.org/grautiernahrung/grau-importer-woo-theme/downloads/)
5. Goto WordPress > Appearance > Themes > Add New.
6. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
7. Click Activate to use this childtheme right away.

## Usage

This child theme is designed to be used as a starter theme for the WooCommerce grau Webshop used by the grau importers. Several options can be changed by using the theme customizer. More settings will be fixed though to preserve the default grau GmbH look and feel. 

There are a few plugins that will enhance the functionality of you installation:

[Advanced Custom Fields](https://wordpress.org/plugins/advanced-custom-fields/)

Will add some custom fields to your product. Just install and activate the plugin to make it work. You will get two extra tabs with info on your productdetailpage: Ingredients and How to Use. It will also give you the possibility to show an info / alert / warning / error message at the productspage. And at last the option to add extra keywords for searching. You can enter misspelled versions of the productname that will not be shown on the frontend but will be used in the search index when you install:

[Relevanssi – A Better Search](https://wordpress.org/plugins/relevanssi/)    

 Relevanssi will allow you to search to more than the usual fields that Wordpress will search through. You can also add custom field names. When you install it add the following settings to > Settings > Relevanssi > Indexing: Custom Fields choose the option all or some and enter: 
 
 _grau_product_tab_use, _grau_product_tab_ingredients, _grau_productsearch_keywords
 
 so that the extra added content will be searched as well. Other information about how to use and configure relevanssi can be found on the: [Relevanssi Website](https://www.relevanssi.com/) it is also possible to order a premium version here. That is not necessary for the functions we are using.
 
[LocoTranslate](https://wordpress.org/plugins/loco-translate/)

If you need to translate any plugin or theme into your own language you can do it with the Locotranslate plugin. It's a great editor for making everything you use on your website to be shown in the right language.

## Shortcodes

[**grau-news-list** items_per_page="6" category="1"]

use the grau-news-list shortcode to show a list of newsarticles. The usual posttype post is being used here. You can use a different items per page as the default items per page from the Wordpress settings. You can also filter on a category. Use the category-id to show only items from this category. You can find the category-id by edit the category and look in the page url. The ID can be found at the section *tag_ID=4*. In this example the category ID would be 4.  

## Note

Using this templates requires knowledge of how to install themes and plugins on a Wordpress system. Since these theme is stored in Git you can also choose to pull it directly from git so you can easily pull new updates into your installation. We will not give any support on programming or installation issues other than issues occured by errors / bugs in our theme. If you want to report errors / bugs / wishes you can use the Issue page on bitbucket:
[Report theme bugs here](https://bitbucket.org/grautiernahrung/grau-importer-woo-theme/issues)  
  



