<?php
/**
 * Define some shortcodes for the grau theme
 */
    function register_grautheme_shortcodes() {
        add_shortcode('grau-news-list', 'grau_news_list_sc');
        add_shortcode('grau-date', 'grau_date_sc');
    }

/**
 * Returns a list of items of posttype post
 * @param $atts
 * @return string
 */
    function grau_news_list_sc($atts) {

        extract(shortcode_atts(array(
            'items_per_page' => 24,
            'category' => ''
        ), $atts));

        $return_html = '';

        // is there a pager active?
        $current_page = get_query_var( 'paged', 1 );
        if ($current_page > 1) {
            $startat = intval($items_per_page) * $current_page;
        } else {
            $startat = 0;
        }

        // The Query
        $args = array(
            'post_type' => 'post',
            'order_by' => 'date',
            'order_desc' => 'post',
            'status' => 'publish',
            'posts_per_page' => $items_per_page,
            'offset' => $startat
        );

        // check if there is a limit on category
        if (isset($category) && intval($category) > 0) {
            $args["cat"] = $category;
        }

        $the_query = new WP_Query( $args );

        // The Loop
        if ( $the_query->have_posts() ) {
            $return_html .= '<div class="grau-news-list">';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();

                $return_html .= '<article class="grau-news-list-item" id="news-item-' . get_the_ID() . '">';

                $return_html .= '<div class="news-inner">';
                $return_html .= '<a href="' . get_the_permalink() . '">';
                    $return_html .= '<div class="news-image" style="background-image:url(' . get_the_post_thumbnail_url(get_the_ID(), 'medium') . ');background-position:center center; background-size:cover;height:250px;"></div>';
                    $return_html .= '<div class="news-content"><h3>' . get_the_title() . '</h3>' . get_the_excerpt() . '</div>';
                $return_html .= '</a>';
                $return_html .= '</div>';
                $return_html .= '<div class="news-meta">';
                    $return_html .= date_i18n(get_option('date_format')) . ' | ';
                    $return_html .= __('Categories','grauwoo') . ': '. get_the_category_list(', ');
                    $return_html .= '<div class="post-readmore">' . '<a href="' . get_the_permalink() . '">' . __('Read more', 'grauwoo') . '</a></div>';
                $return_html .= '</div>';

                $return_html .= '</article>';

            }
            $return_html .= '</div>';
            $return_html .= grau_page_navi($the_query->max_num_pages);
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            $return_html .= __('No newsitems have been found','grauwoo'); // no posts found
        }

        return $return_html;
    }

/**
 * Return the current date
 */
    function grau_date_sc($atts) {

        extract(shortcode_atts(array(
            'format' => 'd-m-Y',
        ), $atts));

        return date_i18n($format);

    }