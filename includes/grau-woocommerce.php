<?php
/**
 * Change the way a variation price is being displayed
 * @param $price
 * @param $product
 * @return string
 */
    function grau_variation_price( $price, $product ) {

        $prefix_from = sprintf('%s: ', __('From', 'grauwoo'));
        $prefix_price = sprintf('%s: ', __('Current price', 'grauwoo'));

        $min_price_regular = $product->get_variation_regular_price( 'min', true );
        $min_price_sale    = $product->get_variation_sale_price( 'min', true );
        $max_price = $product->get_variation_price( 'max', true );
        $min_price = $product->get_variation_price( 'min', true );

        $price = ( $min_price_sale == $min_price_regular ) ?
            wc_price( $min_price_regular ) :
            '<del>' . wc_price( $min_price_regular ) . '</del>' . '<ins>' . wc_price( $min_price_sale ) . '</ins>';

        return ( $min_price == $max_price ) ?
            sprintf('%s%s', $prefix_price, $price) :
            sprintf('%s%s', $prefix_from, $price);

    }

/**
 * Change the amount of related products
 * @param $args
 * @return mixed
 */
    function grau_related_products_args( $args ) {
        $args['posts_per_page'] = 3; // 4 related products
        $args['columns'] = 1; // arranged in 2 columns
        return $args;
    }

/**
 * @param $cart_fragments
 * @return mixed
 */
    function woocommerce_header_add_to_cart_custom_fragment( $cart_fragments ) {
        global $woocommerce;
        ob_start();
        ?>
        <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View cart', 'grauwoo'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'grauwoo'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
        <?php
        $cart_fragments['a.cart-contents'] = ob_get_clean();
        return $cart_fragments;
    }

/**
 * No title on the description tab because the tab title is more or less the title
 * @param $title
 * @return string
 */
    function grau_descr_tab_header($title) {
        return '';
    }

/**
 * Add two new tabs to the product on the product detailpage
 * @param $tabs
 * @return mixed
 */
    function grau_new_product_tabs( $tabs ) {
        // Adds the new tab
        $tabs['grau_ingredients_tab'] = array(
            'title'     => __( 'Ingredients', 'grauwoo' ),
            'priority'  => 50,
            'callback'  => 'grau_product_tab_ingredients'
        );
        // Adds the new tab
        $tabs['grau_useproduct_tab'] = array(
            'title'     => __( 'How to use', 'grauwoo' ),
            'priority'  => 60,
            'callback'  => 'grau_product_tab_use'
        );
    
        unset( $tabs['additional_information'] );  	// Remove the additional information tab
    
        return $tabs;
    }

/**
 * Show an extra tab at the product detail page: Ingredients
 */
    function grau_product_tab_ingredients() {
        // The new tab content
        $prod_id = get_the_ID();
        $tabcontent = get_field( '_grau_product_tab_ingredients', $prod_id,true);
        echo $tabcontent;
    }

/**
 * Show an extra tab at the product detail page: How to use
 */
    function grau_product_tab_use() {
        // The new tab content
        $prod_id = get_the_ID();
        $tabcontent = get_field('_grau_product_tab_use', $prod_id, true);
        echo $tabcontent;
    }

