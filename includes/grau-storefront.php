<?php
/**
 * Overwrite storefront functions that show productlists on the homepage
 */
    function storefront_product_categories() { return ''; }
    function storefront_recent_products() { return ''; }
    function storefront_featured_products() { return ''; }
    function storefront_on_sale_products() { return ''; }
    function storefront_popular_products() { return ''; }
    function storefront_best_selling_products() { return ''; }

/**
 * Add div for showing logo + background above footer
 */
    function grau_top_footer() {
        echo '<div id="grau-top-footer"><div class="footer-logo"></div></div>';
    }

/**
 * Add two widget areas below the footer - often used for copyright, payment methods, etc
 */
    function grau_bottom_footer() {
        ?>
        <div id="grau-bottom-footer">
            <div class="col-full">
                <div class="block lower-footer-widget-1"><?php dynamic_sidebar( 'lowerfooterleft' ); ?></div>
                <div class="block lower-footer-widget-2"><?php dynamic_sidebar( 'lowerfooterright' ); ?></div>
            </div>
        </div>
        <!--graucheck-->
        <?php
    }

/**
 * Remove storefront credits from the footer
 */
    function storefront_credit() {
        echo '';
    }

/**
 * Display the post content
 *
 * @since 1.0.0
 */
    function grau_homepage_banner() {
        if (is_front_page()) {
            ?>
            <section class="homepage_slider" id="grau_homepage_slider" role="banner" style="<?php storefront_header_styles(); ?>width:100%;margin:0;padding:0;height:280px;background-position:center center;background-size: cover;background-repeat:no-repeat;">
            </section>
            <?php
        }
    }

/**
 * Init two new sidebars for below the footer, often used to show paymentmethods, copyrights, etc
 */
    function grau_register_sidebars() {

        register_sidebar(array(
            'id' => 'lowerfooterleft',
            'name' => __('Lower Footer Left', 'grauwoo'),
            'description' => __('Below footer', 'grauwoo'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="widgettitle">',
            'after_title' => '</h4>',
        ));

        register_sidebar(array(
            'id' => 'lowerfooterright',
            'name' => __('Lower Footer Right', 'grauwoo'),
            'description' => __('Below footer', 'grauwoo'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="widgettitle">',
            'after_title' => '</h4>',
        ));

    }

/**
 * Show related information to this product
 * NOTE: Works only in combination with the HOKAMIX plugin for extra content types and information combined with the products
 */
    function grau_show_related_content_items() {

        $postid = get_the_ID();

        if (function_exists('get_terms_id_list')) {
            $problemareas = get_terms_id_list($postid, 'problemareas');
            $animals = get_terms_id_list($postid, 'animal');
            $brands = get_terms_id_list($postid, 'brand');

            echo '<div class="hm-related-items cf">';
            echo '<div class="cf"><div class="m-all t-1of2 d-1of2">';
            echo get_related_problems(10, $problemareas, $animals, $brands);
            echo '</div>';
            echo '<div class="m-all t-1of2 d-1of2">';
            echo get_related_faqs(10, $problemareas, $animals, $brands);
            echo '</div></div>';
            echo '<h3>' . __('Related experiences', 'grauwoo') . '</h3>';
            echo get_related_experiences(2, $problemareas, $animals, $brands);
            echo '</div>';
        }

    }

/**
 * Site branding wrapper and display override
 * Load logo from childtheme - no text as logo
 *
 * @since  1.0.0
 * @return void
 */
    function storefront_site_branding() {
        ?>
        <div class="site-branding">
            <div id="sitelogo"><a href="/"><img src="<?php echo GRAU_THEME_URL; ?>/assets/images/grau_logo.png" class="custom-logo" alt="grau GmbH logo"></a></div>
        </div>
        <?php
    }

/**
 * Move the checkout button above the totals in the cart
 * @param $checkout
 */
    function grau_move_proceed_button( $checkout ) {
        echo '<a href="' . wc_get_checkout_url() . '" class="checkout-button button alt wc-forward" >' . __( 'Proceed to Checkout', 'grauwoo' ) . '</a>';
    }

    function grau_template_loop_product_descr_containerstart() {
        echo '<div class="hm-productlist-content-container">';
    }

/**
 * Show the productdescription also in the list with products, add an extra div to show a gradient overlay for products that have a
 * longer description than there is room for
 */
    function grau_template_loop_product_descr() {
        $description = '';
        $description .= '<div class="product-excerpt">' . get_the_excerpt() . '</div>';
        $description .= '<div class="bottom-gradient"></div>';
        $description .= '</div>';
        echo $description;
    }


/**
 * Add a special text to a product (alert/warning/info)
 * @return string
 */
    function grau_product_special_text() {

        $postid = get_the_ID();
        $showalert = get_field('_grau_product_show_alert', $postid);
        $alerttype = get_field('_grau_product_alert_type', $postid);
        $alerttext = get_field('_grau_product_alert_text', $postid);

        if ($showalert !== false) {
            echo '<p class="alert-' . $alerttype .'">' .  $alerttext . '</p>';
        }

    }

/**
 * Display the post meta
 *
 * @since 1.0.0
 */
    function storefront_post_meta() {
        if ( 'post' !== get_post_type() ) {
            return;
        }

        // Posted on.
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf(
            $time_string,
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date() ),
            esc_attr( get_the_modified_date( 'c' ) ),
            esc_html( get_the_modified_date() )
        );

        $output_time_string = sprintf( '<a href="%1$s" rel="bookmark">%2$s</a>', esc_url( get_permalink() ), $time_string );

        $posted_on = '
			<span class="posted-on">' .
            /* translators: %s: post date */
            sprintf( __( 'Posted on %s', 'grauwoo' ), $output_time_string ) .
            '</span>';

        // Author.
        $author = '';
//        $author = sprintf(
//            '<span class="post-author">%1$s <a href="%2$s" class="url fn" rel="author">%3$s</a></span>',
//            __( 'by', 'storefront' ),
//            esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
//            esc_html( get_the_author() )
//        );

        // Comments.
        $comments = '';

        if ( ! post_password_required() && ( comments_open() || 0 !== intval( get_comments_number() ) ) ) {
            $comments_number = get_comments_number_text( __( 'Leave a comment', 'grauwoo' ), __( '1 Comment', 'grauwoo' ), __( '% Comments', 'grauwoo' ) );

            $comments = sprintf(
                '<span class="post-comments">&mdash; <a href="%1$s">%2$s</a></span>',
                esc_url( get_comments_link() ),
                $comments_number
            );
        }

        echo wp_kses(
            sprintf( '%1$s %2$s %3$s', $posted_on, $author, $comments ), array(
                'span' => array(
                    'class' => array(),
                ),
                'a'    => array(
                    'href'  => array(),
                    'title' => array(),
                    'rel'   => array(),
                ),
                'time' => array(
                    'datetime' => array(),
                    'class'    => array(),
                ),
            )
        );
    }

/**
 * Display the post taxonomies
 *
 * @since 2.4.0
 */
    function storefront_post_taxonomy() {
        /* translators: used between list items, there is a space after the comma */
        $categories_list = get_the_category_list( __( ' ', 'grauwoo' ) );

        /* translators: used between list items, there is a space after the comma */
        $tags_list = get_the_tag_list( '', __( ' ', 'grauwoo' ) );
        ?>

        <aside class="entry-taxonomy">
            <?php if ( $categories_list ) : ?>
                <div class="cat-links">
                    <?php _e('Categorie(s)','grauwoo'); ?>: <?php echo wp_kses_post( $categories_list ); ?>
                </div>
            <?php endif; ?>

            <?php if ( $tags_list ) : ?>
                <div class="tags-links">
                    <?php echo wp_kses_post( $tags_list ); ?>
                </div>
            <?php endif; ?>
        </aside>

        <?php
    }