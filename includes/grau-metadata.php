<?php

// enhance the woocommerce product posttype with some extra fields when woocommerce is active and
// acf has been installed
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

    if( function_exists('acf_add_local_field_group') ) {

        // hide ACF from admin menu
        add_filter('acf/settings/show_admin', '__return_false');

        // define new fields for WooCommerce product
        acf_add_local_field_group(array(
            'key' => 'grau_producttabs_extra_info',
            'title' => __('Producttabs extra info', 'grauwoo'),
            'fields' => array(
                array(
                    'key' => '_grau_product_tab_ingredients',
                    'label' => __('Ingredients', 'grauwoo'),
                    'name' => '_grau_product_tab_ingredients',
                    'type' => 'wysiwyg',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'tabs' => 'all',
                    'toolbar' => 'full',
                    'media_upload' => 0,
                    'delay' => 0,
                ),
                array(
                    'key' => '_grau_product_tab_use',
                    'label' => __('How to use', 'grauwoo'),
                    'name' => '_grau_product_tab_use',
                    'type' => 'wysiwyg',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'tabs' => 'all',
                    'toolbar' => 'full',
                    'media_upload' => 0,
                    'delay' => 0,
                ),
                array(
                    'key' => '_grau_product_alert_text',
                    'label' => __('Alert Text', 'grauwoo'),
                    'name' => '_grau_product_alert_text',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => '_grau_product_alert_type',
                    'label' => __('Alert Type', 'grauwoo'),
                    'name' => '_grau_product_alert_type',
                    'type' => 'select',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'choices' => array(
                        'info' => __('Info', 'grauwoo'),
                        'warning' => __('Warning', 'grauwoo'),
                        'error' => __('Error', 'grauwoo'),
                        'note' => __('Note', 'grauwoo'),
                    ),
                    'default_value' => array(
                        0 => 'info',
                    ),
                    'allow_null' => 0,
                    'multiple' => 0,
                    'ui' => 0,
                    'ajax' => 0,
                    'return_format' => 'value',
                    'placeholder' => '',
                ),
                array(
                    'key' => '_grau_product_show_alert',
                    'label' => __('Show Alert', 'grauwoo'),
                    'name' => '_grau_product_show_alert',
                    'type' => 'true_false',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => __('Show alert', 'grauwoo'),
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ),
                array(
                    'key' => '_grau_productsearch_keywords',
                    'label' => __('Search Keywords', 'grauwoo'),
                    'name' => '_grau_productsearch_keywords',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'default_value' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'product',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }

}