<?php
/*********************
RANDOM CLEANUP ITEMS
 *********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
    function grau_filter_ptags_on_images($content){
        return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
    }

// This removes the annoying […] to a Read More link
    function grau_excerpt_more($more) {
        global $post;
        // edit here if you like
        return '...  <a class="excerpt-read-more" href="'. get_permalink( $post->ID ) . '" title="'. __( 'Read ', 'grauwoo' ) . esc_attr( get_the_title( $post->ID ) ).'">'. __( 'Read more &raquo;', 'grauwoo' ) .'</a>';
    }

/**
 * Remove Read More Links from all excerpts
 * @param $more
 * @return string
 */
    function grau_custom_excerpt_more( $more ) {
        return '…';
    }

/**
 * Grau default function for page navigation
 */
    function grau_page_navi($pages = '', $range = 4) {

        $showitems = ($range * 2)+1;
        $label_page = __('Page', 'grauwoo');
        $label_page_of = __('of', 'grauwoo');
        $label_first = __('First', 'grauwoo');
        $label_previous = __('Previous', 'grauwoo');
        $label_next = __('Next', 'grauwoo');
        $label_last = __('Last', 'grauwoo');

        $return_html = '';

        global $paged;
        if (empty($paged)) $paged = 1;

        if($pages == '') {
            global $wp_query;
            $pages = $wp_query->max_num_pages;
            if(!$pages) { $pages = 1; }
        }

        if (1 != $pages) {
            $return_html .= "<div class=\"grau-pagination\"><div class=\"info\">$label_page ".$paged." $label_page_of ".$pages."</div>";
            if($paged > 2 && $paged > $range+1 && $showitems < $pages) $return_html .= "<a href='".get_pagenum_link(1)."'>&laquo; $label_first</a>";
            if($paged > 1 && $showitems < $pages) $return_html .=  "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; $label_previous</a>";

            for ($i=1; $i <= $pages; $i++) {
                if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
                    $return_html .= ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
                }
            }

            if ($paged < $pages && $showitems < $pages) { $return_html .= "<a href=\"".get_pagenum_link($paged + 1)."\">$label_next &rsaquo;</a>"; }
            if ($paged < $pages-1 && $paged+$range-1 < $pages && $showitems < $pages) { $return_html .= "<a href='".get_pagenum_link($pages)."'>$label_last &raquo;</a>"; }
            $return_html .= "</div>\n";
        }

        // return the navigation
        return $return_html;
    }