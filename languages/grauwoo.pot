#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: grau WooCommerce Theme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-21 20:26+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.2.0; wp-5.0.3"

#: category.php:43 tag.php:43 includes/grau-shortcodes.php:59
msgid "Categories"
msgstr ""

#: category.php:44 tag.php:44 includes/grau-shortcodes.php:60
msgid "Read more"
msgstr ""

#: includes/grau-woocommerce.php:10
msgid "From"
msgstr ""

#: includes/grau-woocommerce.php:11
msgid "Current price"
msgstr ""

#: includes/grau-woocommerce.php:47
msgid "View cart"
msgstr ""

#: includes/grau-woocommerce.php:47
#, php-format
msgid "%d item"
msgid_plural "%d items"
msgstr[0] ""
msgstr[1] ""

#: includes/grau-woocommerce.php:70 includes/grau-metadata.php:19
msgid "Ingredients"
msgstr ""

#: includes/grau-woocommerce.php:76 includes/grau-metadata.php:38
msgid "How to use"
msgstr ""

#: includes/grau-storefront.php:62
msgid "Lower Footer Left"
msgstr ""

#: includes/grau-storefront.php:63 includes/grau-storefront.php:73
msgid "Below footer"
msgstr ""

#: includes/grau-storefront.php:72
msgid "Lower Footer Right"
msgstr ""

#: includes/grau-storefront.php:102
msgid "Related experiences"
msgstr ""

#: includes/grau-storefront.php:129
msgid "Proceed to Checkout"
msgstr ""

#. %s: post date
#: includes/grau-storefront.php:196
#, php-format
msgid "Posted on %s"
msgstr ""

#: includes/grau-storefront.php:212
msgid "Leave a comment"
msgstr ""

#: includes/grau-storefront.php:212
msgid "1 Comment"
msgstr ""

#: includes/grau-storefront.php:212
msgid "% Comments"
msgstr ""

#. used between list items, there is a space after the comma
#: includes/grau-storefront.php:246 includes/grau-storefront.php:249
msgid " "
msgstr ""

#: includes/grau-storefront.php:255
msgid "Categorie(s)"
msgstr ""

#: includes/grau-shortcodes.php:71
msgid "No newsitems have been found"
msgstr ""

#: includes/grau-metadata.php:15
msgid "Producttabs extra info"
msgstr ""

#: includes/grau-metadata.php:57
msgid "Alert Text"
msgstr ""

#: includes/grau-metadata.php:76
msgid "Alert Type"
msgstr ""

#: includes/grau-metadata.php:88
msgid "Info"
msgstr ""

#: includes/grau-metadata.php:89
msgid "Warning"
msgstr ""

#: includes/grau-metadata.php:90
msgid "Error"
msgstr ""

#: includes/grau-metadata.php:91
msgid "Note"
msgstr ""

#: includes/grau-metadata.php:105
msgid "Show Alert"
msgstr ""

#: includes/grau-metadata.php:116
msgid "Show alert"
msgstr ""

#: includes/grau-metadata.php:124
msgid "Search Keywords"
msgstr ""

#: includes/grau-functions.php:15
msgid "Read "
msgstr ""

#: includes/grau-functions.php:15
msgid "Read more &raquo;"
msgstr ""

#: includes/grau-functions.php:33
msgid "Page"
msgstr ""

#: includes/grau-functions.php:34
msgid "of"
msgstr ""

#: includes/grau-functions.php:35
msgid "First"
msgstr ""

#: includes/grau-functions.php:36
msgid "Previous"
msgstr ""

#: includes/grau-functions.php:37
msgid "Next"
msgstr ""

#: includes/grau-functions.php:38
msgid "Last"
msgstr ""

#. Name of the theme
msgid "grau WooCommerce Theme"
msgstr ""

#. Description of the theme
msgid ""
"grau 2019 Woocommerce Theme based on Storefront. Please do not delete the "
"storefront theme since this is a child Theme"
msgstr ""

#. URI of the theme
#. Author URI of the theme
msgid "https://www.grau-tiernahrung.de"
msgstr ""

#. Author of the theme
msgid "Vincent Bourgonje"
msgstr ""
