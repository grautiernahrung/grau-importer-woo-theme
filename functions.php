<?php

define('GRAU_THEME_URL', get_stylesheet_directory_uri());
define('GRAU_THEME_DIR', get_stylesheet_directory());
define('GRAU_THEME_FILE', basename(__FILE__));
define('GRAU_THEME_LANGUAGE', GRAU_THEME_DIR . '/languages');
define('GRAU_THEME_DEBUG', false);

// Load text domain for translations
    $languageloaded = load_theme_textdomain('grauwoo',  GRAU_THEME_LANGUAGE);

/**
 * Include necessary theme components
 */
    require_once(GRAU_THEME_DIR . '/includes/grau-functions.php');           // Other functions
    require_once(GRAU_THEME_DIR . '/includes/grau-shortcodes.php');          // Shortcodes
    require_once(GRAU_THEME_DIR . '/includes/grau-storefront.php');          // Changes to storefront
    require_once(GRAU_THEME_DIR . '/includes/grau-woocommerce.php');         // Changes to woocommerce
    require_once(GRAU_THEME_DIR . '/includes/grau-metadata.php');            // Extra fields definition

// enqueue base scripts and styles
    add_action( 'wp_enqueue_scripts', 'grau_scripts_and_styles', 999999);
    add_action( 'storefront_before_footer', 'grau_top_footer', 1);
    add_action( 'storefront_after_footer', 'grau_bottom_footer', 99);
    add_action( 'storefront_content_top', 'grau_homepage_banner', 1);
    add_action( 'woocommerce_single_product_summary', 'grau_product_special_text', 25 );
    add_action( 'woocommerce_shop_loop_item_title', 'grau_template_loop_product_descr_containerstart', 1 );
    add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_rating', 20 );
    add_action( 'woocommerce_after_single_product', 'grau_show_related_content_items', 10 );
    add_action( 'customize_register', 'grau_customize_register',50 );
    add_action( 'init', 'register_grautheme_shortcodes' );
    add_action( 'widgets_init', 'grau_register_sidebars');
    //add_action( 'woocommerce_before_cart_totals', 'grau_move_proceed_button' );
    //add_action( 'woocommerce_shop_loop_item_title', 'grau_template_loop_product_descr', 30 );

// inherited from the hokamix theme
    add_filter( 'woocommerce_variable_sale_price_html', 'grau_variation_price', 10, 2 );
    add_filter( 'woocommerce_variable_price_html', 'grau_variation_price', 10, 2 );
    add_filter( 'woocommerce_output_related_products_args', 'grau_related_products_args' );
    add_filter( 'woocommerce_product_description_heading', 'grau_descr_tab_header' );
    add_filter( 'woocommerce_product_tabs', 'grau_new_product_tabs', 98 );
    add_filter( 'add_to_cart_custom_fragments', 'woocommerce_header_add_to_cart_custom_fragment');
    add_filter( 'excerpt_more', 'grau_custom_excerpt_more' ); // Remove Read More Links from all excerpts

// remove the homepage actions for showing products - use elementor blocks in stead of the fixed blocks
    remove_action( 'homepage', 'storefront_product_categories',20 );
    remove_action( 'homepage', 'storefront_recent_products', 30 );
    remove_action( 'homepage', 'storefront_featured_products', 40 );
    remove_action( 'homepage', 'storefront_popular_products', 50 );
    remove_action( 'homepage', 'storefront_on_sale_products', 60 );
    remove_action( 'homepage', 'storefront_best_selling_products', 70 );

// cleanup the page head
    remove_action( 'wp_head', 'rsd_link' );
    // windows live writer
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // previous link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    // start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    // links for adjacent posts
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    // WP version
    remove_action( 'wp_head', 'wp_generator' );
    remove_action( 'wp_head', 'generator' );


function grau_return_void() {
    return '';
}



// load custom styles and scripts
function grau_scripts_and_styles() {

    global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    if (!is_admin()) {

        $style_url = get_stylesheet_directory_uri() . '/assets/css/grau-woo-style.css';
        $style_dir = get_stylesheet_directory() . '/assets/css/grau-woo-style.css';
        $js_url = get_stylesheet_directory_uri() . '/assets/js/scripts.min.js';
        $js_dir = get_stylesheet_directory() . '/assets/js/scripts.min.js';

        // register main stylesheet
        wp_enqueue_style( 'grau-woo-stylesheet', $style_url, array(), date_i18n("YmdHis",filemtime($style_dir)), 'all' );

        //adding scripts file in the footer
        wp_register_script( 'grau-woo-base-js', $js_url, array( 'jquery' ), date_i18n("YmdHis",filemtime($js_dir)), true );
        wp_enqueue_script( 'grau-woo-base-js' );

    }
}

/**
 * Remove those items from the customizer that we have declared fixed in this child theme (most of them are colors and logo)
 * @param $wp_customize
 */
    function grau_customize_register( $wp_customize ) {
        //All our sections, settings, and controls will be added here
        $wp_customize->remove_section( 'title_tagline');
        $wp_customize->remove_control( 'storefront_heading_color');
        $wp_customize->remove_control( 'storefront_text_color');
        $wp_customize->remove_control( 'storefront_accent_color');
        $wp_customize->remove_control( 'storefront_header_background_color');
        $wp_customize->remove_control( 'storefront_header_text_color');
        $wp_customize->remove_control( 'storefront_header_link_color');
        $wp_customize->remove_control( 'storefront_footer_background_color');
        $wp_customize->remove_control( 'storefront_footer_heading_color');
        $wp_customize->remove_control( 'storefront_footer_text_color');
        $wp_customize->remove_control( 'storefront_footer_link_color');
        $wp_customize->remove_control( 'storefront_button_background_color');
        $wp_customize->remove_control( 'storefront_button_text_color');
        $wp_customize->remove_control( 'storefront_button_alt_background_color');
        $wp_customize->remove_control( 'storefront_button_alt_text_color');
        $wp_customize->remove_section( 'colors');
        $wp_customize->remove_section( 'background_image');
        $wp_customize->remove_section( 'custom_css');
    }
