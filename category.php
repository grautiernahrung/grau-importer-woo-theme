<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package storefront
 */

get_header();
$return_html = '';
global $wp_query;
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php if ( have_posts() ) : ?>

                <header class="page-header">
                    <?php
                    the_archive_title( '<h1 class="page-title">', '</h1>' );
                    the_archive_description( '<div class="taxonomy-description">', '</div>' );
                    ?>
                </header><!-- .page-header -->
                <div class="grau-news-list">
                <?php
                while ( have_posts() ) {

                    the_post();

                    $return_html .= '<article class="grau-news-list-item" id="news-item-' . get_the_ID() . '">';

                    $return_html .= '<div class="news-inner">';
                    $return_html .= '<a href="' . get_the_permalink() . '">';
                    $return_html .= '<div class="news-image" style="background-image:url(' . get_the_post_thumbnail_url(get_the_ID(),
                            'medium') . ');background-position:center center; background-size:cover;height:250px;"></div>';
                    $return_html .= '<div class="news-content"><h3>' . get_the_title() . '</h3>' . get_the_excerpt() . '</div>';
                    $return_html .= '</a>';
                    $return_html .= '</div>';
                    $return_html .= '<div class="news-meta">';
                    $return_html .= date_i18n(get_option('date_format')) . ' | ';
                    $return_html .= __('Categories', 'grauwoo') . ': ' . get_the_category_list(', ');
                    $return_html .= '<div class="post-readmore">' . '<a href="' . get_the_permalink() . '">' . __('Read more',
                            'grauwoo') . '</a></div>';
                    $return_html .= '</div>';

                    $return_html .= '</article>';
                }



                $return_html .= grau_page_navi($wp_query->max_num_pages);
                echo $return_html;

            else :

                get_template_part( 'content', 'none' );

            endif;
            ?>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
do_action( 'storefront_sidebar' );
get_footer();